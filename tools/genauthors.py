#!/usr/bin/env python3
#
#  genauthors.py
#
#  Copyright 2015 Karl Lindén <karl.linden.887@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

# Generate authors cache file from input files.

from common import *

def main():
    parser = argparse.ArgumentParser(description='Generate authors '
                                                 'cache')
    add_common_arguments(parser)
    args = parser.parse_args()

    exercises, chapters, authors_cache = common_data(args)

    authors = Authors()
    authors += exercises.authors()
    authors += chapters.authors()

    f = open(authors_cache, 'w', encoding='utf8')
    f.write(repr(authors))
    f.close()

    exit(0)

if __name__ == '__main__':
    main()
