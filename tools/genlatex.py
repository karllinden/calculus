#!/usr/bin/env python3
#
#  generate.py
#
#  Copyright 2015 Karl Lindén <karl.linden.887@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import argparse
import os
import subprocess
import sys

import common

header = '''
\\documentclass[a4paper,12pt]{article}

\\usepackage{amsfonts}
\\usepackage{amsmath}
\\usepackage{amssymb}
\\usepackage{amsthm}
\\usepackage{enumerate}
\\usepackage{enumitem}
\\usepackage{float}
\\usepackage{mathtools}
\\usepackage[hidelinks]{hyperref}
\\usepackage{import}
\\usepackage{multicol}
\\usepackage{pst-plot}
\\usepackage{pstricks}

\\usepackage[english]{babel}
\\usepackage[T1]{fontenc}
\\usepackage[latin1]{inputenc}

\\newtheorem{lemma}{Lemma}

\\newlist{abc}{enumerate}{1}
\\setlist[abc]{label*=\\bf(\\alph*)}

\\SetEnumitemKey{twocol}{
  before=\\raggedcolumns\\begin{multicols}{2},
  after=\\end{multicols}}

\\SetEnumitemKey{threecol}{
  before=\\raggedcolumns\\begin{multicols}{3},
  after=\\end{multicols}}

\\renewcommand{\\d}[1]{\\mathrm{d}{#1}}
\\newcommand{\\dd}[1]{\\frac{\\mathrm{d}}{\\d{#1}}}

\\begin{document}
'''

footer = '''
\\end{document}
'''


parser = argparse.ArgumentParser(description='Generate LaTeX file.')
common.add_common_arguments(parser)
parser.add_argument('--answers', '-a', action='store_true',
                    default=False,
                    help='include answers in generated file')
parser.add_argument('--exercises', '-e', action='store_true',
                    default=False,
                    help='include exercises in generated file')
parser.add_argument('--solutions', '-s', action='store_true',
                    default=False,
                    help='include solutions in generated file')
parser.add_argument('--course', '-c', default=None, help='course name')
parser.add_argument('--url', '-r', default=None, help='project url')
parser.add_argument('--output', '-o', default=None, help='output file')
parser.add_argument('--list', '-l', default=None,  help='list file')
args = parser.parse_args()

common.include_answers(args.answers)
common.include_exercises(args.exercises)
common.include_solutions(args.solutions)

exercises, chapters, authors_cache = common.common_data(args)

# Mandatory arguments
if not args.course:
    common.error('--course must be given')
if not args.url:
    common.error('--url must be given')
if not args.output:
    common.error('--output must be given')
if not args.list:
    common.error('--list must be given')


# Try to figure out if we are being run in a chapter and find out
# the chapter number if possible.
dirname = os.path.basename(os.getcwd())
chap = None
if '-' in dirname:
    parts = dirname.split('-')
    if len(parts) == 2 and \
       parts[0].isnumeric() and \
       parts[1] == 'chap':
        chap = int(parts[0])


# Now that the chapter is known, generate the title.
inc_ex  = common.inc_ex
inc_sol = common.inc_sol
inc_ans = common.inc_ans
title = ''
if inc_ex:
    title += 'Exercises'
if inc_sol:
    if inc_ex:
        if inc_ans:
            title += ', '
        else:
            title += ' and '
        title += 'solutions'
    else:
        title += 'Solutions'
if inc_ans:
    if inc_ex or inc_sol:
        title += ' and answers'
    else:
        title += 'Answers'
if chap:
    title += ' to chapter ' + str(chap)


# Generate the list file.
output = open(args.list, 'w')
output.write('\\clearpage\n')

output.write('\\section*{%s}\n\n' % title)
if not exercises:
    output.write('No exercises.\n')
else:
    output.write('\\begin{itemize}\n\n')
    for e in exercises:
        output.write('\n')
        output.write('  \\item[\\textbf{')
        if chap:
            output.write(str(chap) + '.')
        output.write(str(e) + '}]\n')
        for l in e().split('\n'):
            if l:
                output.write('  ' + l + '\n')
            else:
                output.write('\n')
    output.write('\n\\end{itemize}\n')
output.close()


# Generate the main LaTeX file.
output = open(args.output, 'w')

# Generate the title page.
authors = common.Authors()
authors += exercises.authors()
authors += chapters.authors()

# Before writing the title page, find out the git revision and whether
# the working directory is clean.
revision = subprocess.check_output('git rev-parse --short HEAD',
                                   shell=True, universal_newlines=True)
revision = str(revision).strip('\n')
status = subprocess.check_output('git status --short', shell=True,
                                 universal_newlines=True)
status = str(status).strip('\n')

output.write(header)

output.write('\\title{%s \\\\ %s}\n' % (title, args.course))
output.write('\\author{\n')
for line in authors.latex().split('\n'):
    if line:
        output.write('  ' + line + '\n')
output.write('}\n')
output.write('\\date{')
output.write(revision)
if status:
    output.write(' (dirty)')
output.write('}\n')
output.write('\\maketitle\n')

output.write( \
'''
\\vfill

\\begin{center}
  A copy of this project can be downloaded in its source form from the
  project page at \\\\
  \\url{%s}.
\\end{center}

\\clearpage

\\noindent Copyright {\\textcopyright} %s %s.
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.
You should have received a copy of this license along with this
document.
A copy of this license can also be viewed at the project page and at the
license page on \\texttt{gnu.org}.

Project page: \\url{%s}

License page: \\url{https://www.gnu.org/licenses/fdl.html}.
''' % (args.url, authors.years(), authors.latexnames(), args.url))

# Input list files.
if exercises or not chapters:
    output.write('\n\\input{%s}\n' % args.list)
if chapters:
    for c in chapters:
        output.write('\n\\subimport*{%s/}{%s}\n' %
                     (c.subdir, args.list))

output.write(footer)
output.close()
